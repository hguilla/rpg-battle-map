import * as firebase from 'firebase/app';
import 'firebase/analytics';
import 'firebase/auth';
import 'firebase/firestore';

var firebaseConfig = {
  apiKey: "AIzaSyCFEGUDCuT4GH3QUNSYY6dAdquP583Oo4k",
  authDomain: "rpg-battle-map.firebaseapp.com",
  databaseURL: "https://rpg-battle-map.firebaseio.com",
  projectId: "rpg-battle-map",
  storageBucket: "rpg-battle-map.appspot.com",
  messagingSenderId: "734228851478",
  appId: "1:734228851478:web:93b6a55875d9bc69cc3e37",
  measurementId: "G-GDBW8L4B1M"
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const db = firebase.firestore();
const auth = firebase.auth();

// collections
const mapsCollection = db.collection('maps');
const usersCollection = db.collection('users');

export {
    db,
    auth,
    mapsCollection,
    usersCollection
};

import Vue from "vue";
import Vuex from "vuex";
import * as firebase from '../firebase'
import router from '../router/index'

Vue.use(Vuex);

firebase.mapsCollection.orderBy('createdOn', 'desc').onSnapshot(snapshot => {
  const mapsArray = [];

  snapshot.forEach(doc => {
    const map = doc.data();
    map.id = doc.id;
    mapsArray.push(map);
  });

  store.commit('setMaps', mapsArray);
});

const store = new Vuex.Store({
    state: {
      currentMap: null,
      maps: [],
      userProfile: {}
    },
    mutations: {
        setCurrentMap(state, map) {
          state.currentMap = map;
        },

        setBackground(state, background) {
          state.currentMap.background = background;
        },

        setDimensions(state, dimensions) {
          state.currentMap.ncols = dimensions.ncols;
          state.currentMap.nrows = dimensions.nrows;
        },

        setMaps(state, val) {
          state.maps = val;
        },

        setUserProfile(state, val) {
          state.userProfile = val;
        },
    },
    actions: {
        async createMap($store, map) {
          await firebase.mapsCollection.add({
            createdOn: new Date(),
            name: map.name,
            ncols: map.ncols,
            nrows: map.nrows,
            createdById: firebase.auth.currentUser.uid
          });
          alert(`createMap '${map.name}': ${map.ncols} x ${map.nrows}`);
        },

        async updateMap($store, map) {
          await firebase.mapsCollection.doc(map.id).update(map);

          const map_background = ((map.background && map.background.type === 'FLAT' && `${map.background.type} (${map.background.color})` ) || 'NONE'  );
          const map_message = `updatedMap '${map.name}': ${map.ncols} x ${map.nrows}\n` +
                              `Background: ${map_background}`;
          alert(map_message);
        },

        async login({ dispatch }, form) {
            const { user } = await firebase.auth.signInWithEmailAndPassword(form.email, form.password);

            dispatch('fetchUserProfile', user);
        },

        async logout({ commit }) {
          await firebase.auth.signOut();

          commit('setUserProfile', {});
          router.push('/login');
        },

        async signup({ dispatch }, form ) {
            const { user } = await firebase.auth.createUserWithEmailAndPassword(form.email, form.password);

            await firebase.usersCollection.doc(user.uid).set({
                name: form.name
            });

            dispatch('fetchUserProfile', user);
        },

        async fetchUserProfile({ commit }, user) {
            const userProfile = await firebase.usersCollection.doc(user.uid).get();

            commit('setUserProfile', userProfile.data())

            if ( router.currentRoute.path === '/login' ) {
              router.push('/');
            }
        },

        async setCurrentMapFromId({ commit, state }, mapId) {
          const map = state.maps.find(map => map.id === mapId);
          commit('setCurrentMap', map);
        }
    }
});

export default store;
